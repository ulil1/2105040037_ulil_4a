import 'package:flutter/material.dart';
import 'package:wisbali_app/halaman_daftar.dart';
import 'package:wisbali_app/beranda.dart';
import 'package:wisbali_app/lupa_sandi.dart';
import 'package:firebase_auth/firebase_auth.dart';

class HalamanMasuk extends StatefulWidget {
  const HalamanMasuk({super.key});

  @override
  State<HalamanMasuk> createState() => _HalamanMasukState();
}

class _HalamanMasukState extends State<HalamanMasuk> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  
  Future signIn() async {
    try {
      final credential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('Pengguna tidak ditemukan.');
      } else if (e.code == 'wrong-password') {
        print('Salah kata sandi.');
      }
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Halaman Masuk")),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 15.0),
              Center(
                child: Image.asset(
                  'images/logo.png',
                  width: 135,
                  height: 160,
                ),
              ),
              SizedBox(
                height: 50.0,
              ),
              Center(
                child: Stack(
                  children: [
                    Image.asset(
                      'images/kotak.png',
                      width: 500.0,
                      height: 250.0,
                      fit: BoxFit.fill,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 15.0, left: 15.0, top: 15.0, bottom: 15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("ID Pengguna / Alamat Email"),
                          SizedBox(
                            height: 5.0,
                          ),
                          TextField(
                            controller: emailController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              hintText: "ID Pengguna / Alamat Email...",
                              hintStyle: TextStyle(color: Colors.white),
                              contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                            ),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Text("Kata Sandi"),
                          SizedBox(
                            height: 5.0,
                          ),
                          Center(
                            child: TextField(
                              controller: passwordController,
                              obscureText: true,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                hintText: "Kata Sandi...",
                                hintStyle: TextStyle(color: Colors.white),
                                contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 3.0,
                          ),
                          Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                InkWell(
                                  child: Text(
                                    "Lupa kata sandi?",
                                    style: TextStyle(color: Colors.blue),
                                  ),
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => LupaSandi()),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 3.0,
                          ),
                          Center(
                            child: SizedBox(
                              width: 300,
                              height: 40.0,
                              child: ElevatedButton(
                                onPressed: signIn,
                                child: Text("Masuk"),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Belum punya akun?"),
                  SizedBox(
                    width: 5.0,
                  ),
                  InkWell(
                    child: Text(
                      "Daftar",
                      style: TextStyle(
                        color: Colors.blue,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HalamanDaftar()),
                      );
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("----------------------- Atau masuk dengan -----------------------"),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Center(
                child: Image.asset(
                  'images/google.png',
                  width: 600.0,
                  height: 60.0,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('@WisBali')
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
