import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:wisbali_app/HalamanWisata.dart';
import 'package:wisbali_app/HalamanKuliner.dart';
import 'package:wisbali_app/HalamanHotel.dart';
import 'package:wisbali_app/HalamanPemandu.dart';
import 'package:wisbali_app/HalamanRental.dart';
import 'package:wisbali_app/HalamanPanduan.dart';
import 'package:wisbali_app/HalamanKontak.dart';
import 'package:wisbali_app/HalamanOleh2.dart';

class Beranda extends StatefulWidget {
  const Beranda({Key? key}) : super(key: key);

  @override
  State<Beranda> createState() => _BerandaState();
}

class _BerandaState extends State<Beranda> {
  Future<void> logOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Beranda"),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(20),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "Cari...",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                color: Color(0xFF7290FA),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Pengumuman",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 30),
                    Text(
                      "Dapatkan berbagai macam informasi tempat wisata, Kuliner dan pengetahuan umum yang akan membuatmu nyaman ketika berwisata di Bali",
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: GridView.count(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  crossAxisCount: 4,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20,
                  children: List.generate(8, (index) {
                    String menuName = "menu_${index + 1}";
                    return InkWell(
                      onTap: () {
                        switch (menuName) {
                          case "menu_1":
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => HalamanWisata()),
                            );
                            break;
                          case "menu-2":
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => HalamanKuliner()),
                            );
                            break;
                          case "menu_3":
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => HalamanHotel()),
                            );
                            break;
                          case "menu_4":
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => HalamanPemandu()),
                            );
                            break;
                          case "menu_5":
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => HalamanRental()),
                            );
                            break;
                          case "menu_6":
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => HalamanPanduan()),
                            );
                            break;
                          case "menu_7":
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => HalamanKontak()),
                            );
                            break;
                          case "Menu 8":
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => HalamanOleh2()),
                            );
                            break;
                          default:
                            break;
                        }
                      },
                      child: Container(
                        color: Colors.transparent,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              '/images/menu_${index + 1}.png',
                              width: 100,
                              height: 100,
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "TOP Rekomendasi Wisata",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      height: 150,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 10,
                        itemBuilder: (context, index) {
                          return Container(
                            width: 200,
                            margin: const EdgeInsets.only(right: 10),
                            color: Colors.grey,
                            child: Center(
                              child: Text(
                                "Wisata ${index + 1}",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "TOP Peringkat Wisata",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      height: 150,
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 10,
                        itemBuilder: (context, index) {
                          return Container(
                            width: 200,
                            margin: const EdgeInsets.only(right: 10),
                            color: Colors.grey,
                            child: Center(
                              child: Text(
                                "Wisata ${index + 1}",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Informasi Umum",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: (MediaQuery.of(context).size.width - 60) / 2,
                          height: 100,
                          color: Colors.grey,
                        ),
                        Container(
                          width: (MediaQuery.of(context).size.width - 60) / 2,
                          height: 100,
                          color: Colors.grey,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Statistik Pengunjung",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10),
                    Container(
                      width: MediaQuery.of(context).size.width - 40,
                      height: 200,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: logOut,
        icon: Icon(Icons.logout_outlined),
        label: Text("Keluar"),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 1,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.arrow_back),
            label: "Kembali",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Beranda",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: "Profil",
          ),
        ],
      ),
    );
  }
}
